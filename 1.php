<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>

    <title>Our team</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/demeu.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    <script src="js.js"></script>
    <script>
        $( document ).ready(function(){ 

            var a = false;
            var b = false;
            var c = false;
          $("#hr").on("click",function(){
            if(a==false){
                $("#hrr").css("display","block")
                $("#prr").css("display","none")
                $("#evv").css("display","none")
                a = true;
                b = false;
                c = false;
            }
            else{                
                $("#hrr").css("display","none")
                a = false;
            }

         });
           $("#pr").on("click",function(){
            if(b==false){
                $("#prr").css("display","block")
                $("#hrr").css("display","none")
                $("#evv").css("display","none")
                a = false;
                b = true;
                c = false;
            }
            else{                
                $("#prr").css("display","none")
                b = false;
            }

         });
            $("#ev").on("click",function(){
            if(c==false){
                $("#evv").css("display","block")
                $("#prr").css("display","none")
                $("#hrr").css("display","none")
                c = true;
                a = false;
                b = false;
            }
            else{                
                $("#evv").css("display","none")
                c = false;
            }

         });
        });
    </script>
</head>

<body>

    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">DEMEU</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">About Us</a>
                    </li>
                    <li>
                        <a href="1.php">Our team</a>
                    </li>
                    <li>
                        <a href="use.php">Useful tips</a>
                    </li>
                    <li id="signin" style="display: block;">
                        <a href="contact.php">Sign in/Sign up</a>
                    </li>
                    <li id="blog" style="display: none;">
                        <a href="blog\project.php">Blog</a>
                    </li>
                    <li id="myp" style="display: none;">
                        <a href="user_profile.php">My profile</a>
                    </li>
                    <li id="signout" style="display: none;">
                        <a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <header class="intro-header" style="background-image: url('ppp.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Our team</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="www">
        <input type="submit" value="HR" id="hr">
        <input type="submit" value="PR" id="pr">
        <input type="submit" value="Event" id="ev">
    </div><br><br>
    <div class="site-section border-bottom" id="hrr" style="display: none">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <div class="person text-center">
              <img src="pictures/me.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Aisha</h3>
              <p class="position text-muted">President</p>
              <p class="mb-4"> slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            <div class="person text-center">
              <img src="pictures/lenya.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Aynel</h3>
              <p class="position text-muted">Vice-president</p>
              <p class="mb-4">slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            <div class="person text-center">
              <img src="pictures/roo.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Aruzhan</h3>
              <p class="position text-muted">Vice-president</p>
              <p class="mb-4">slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs</p>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="site-section border-bottom" id="prr" style="display: none">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <div class="person text-center">
              <img src="pictures/kosya.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Aruzhan</h3>
              <p class="position text-muted">PR manager</p>
              <p class="mb-4"> slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            <div class="person text-center">
              <img src="pictures/karu.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Karu</h3>
              <p class="position text-muted">PR manager</p>
              <p class="mb-4">slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            <div class="person text-center">
              <img src="pictures/ayzh.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Ayzhamal</h3>
              <p class="position text-muted">PR manager</p>
              <p class="mb-4">slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs</p>
            </div>
          </div>
        </div>
      </div>
    </div>

        <div class="site-section border-bottom" id="evv" style="display: none">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <div class="person text-center">
              <img src="pictures/das.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Dastan</h3>
              <p class="position text-muted">Event manager</p>
              <p class="mb-4"> slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            <div class="person text-center">
              <img src="pictures/dau.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Dauren</h3>
              <p class="position text-muted">Event manager</p>
              <p class="mb-4">slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs.</p>

            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            <div class="person text-center">
              <img src="pictures/erken.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <h3>Erken</h3>
              <p class="position text-muted">Event manager</p>
              <p class="mb-4">slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs slnkslnsldnvsnlskfs</p>
            </div>
          </div>
        </div>
      </div>
    </div>
     <hr>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="https://www.instagram.com/demeu.aitu/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:demeu@gmail.com">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Astana IT University 2020</p>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
<?php
if(isset($_SESSION['user'])){
    echo '<script language="javascript">';
    echo "document.getElementById('signin').style.display = 'none';";
    echo "document.getElementById('signout').style.display = 'block';";
    echo "document.getElementById('myp').style.display = 'block';";
    echo "document.getElementById('blog').style.display = 'block';";
    echo "</script>";
}