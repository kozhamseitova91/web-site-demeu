<?php
session_start();
  require 'database.php';

  $sql = "SELECT * FROM posts";
  $res = mysqli_query($conn,$sql);
  if($res===false){
    echo mysqli_error($conn);
  }else{
    $posts = mysqli_fetch_all($res,MYSQLI_ASSOC);
  }
 ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Blog</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/demeu.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
      <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    </head>
    <body> <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">DEMEU</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="../index.php">About Us</a>
                    </li>
                    <li>
                        <a href="../1.php">Our team</a>
                    </li>
                    <li>
                        <a href="../use.php">Useful tips</a>
                    </li>
                    <li id="signin" style="display: none;">
                        <a href="../contact.php">Sign in/Sign up</a>
                    </li>
                    <li id="blog" style="display: block;">
                        <a href="#">Blog</a>
                    </li>
                    <li id="myp" style="display: block;">
                        <a href="../user_profile.php">My profile</a>
                    </li>
                    <li id="signout" style="display: block;">
                        <a href="../signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header class="intro-header" style="background-image: url('ppp.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>Blog</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
                <div  class="container">
                    <ul>
                        <?php foreach ($posts as $post):?>
                        <li style="list-style:none;">
                        <h2><?=$post['title'];?></h2>
                        <p><?=$post['content'];?></p>
                        <p><?=$post['publish_date'];?></p>
                        </li>
                        <hr>
                        <?php endforeach; ?>
                    </ul>
                    <div id="ade" style="display: none;">
				        <button><a href="add_post.php">Add post</a></button>
	     		        <button><a href="delete_post.php">Delete post</a></button>
				        <button><a href="edit_post.php">Edit post</a></button>
	                </div>
                </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="https://www.instagram.com/demeu.aitu/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:demeu@gmail.com">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Astana IT University 2020</p>
                </div>
            </div>
        </div>
    </footer>
    </body>
</html>
<?php
if(isset($_SESSION['user'])){
  if($_SESSION['user']['role']=="admin"){
    echo '<script language="javascript">';
    echo "document.getElementById('ade').style.display = 'block';";
    echo '</script>';
 
  }
}
  ?>