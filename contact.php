<?php
    session_start();

    if (isset($_SESSION['user'])) {
        header("Location: user_profile.php");
        return;
    }
?>
<!DOCTYPE html>
<html>

<head>

    <title></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/demeu.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
     <script>
        $(document).ready(function() {
            $("#email").on('keyup', function(){
                event.preventDefault();
                $("errormsg1").show();
                $("errormsg1").text("Loading data");
                $.ajax('checkemail.php', {
                    type: 'POST', 
                    data: { email: $( "#email" ).val()},  
                    accepts: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.message == 'error') {
                          $('#errormsg1').css({'color': 'red'});
                          $('#errormsg1').text("Account is reserved!");
                          $('#errormsg1').show();
                        }else{
                          $('#errormsg1').css({'color': 'green'});
                          $('#errormsg1').text("Account is free");
                          $('#errormsg1').show();
                        }
                    }
                  });
                });
                  $("#regis").click(function(){
                      event.preventDefault();
                      $.ajax('registration.php', {
                          type: 'POST',  // http method
                          data: { email: $( "#email" ).val(),
                              name:  $( "#name" ).val(),
                              surname:  $( "#surname" ).val(),  
                              password: $("#password").val(),
                              role: $("#role").val()}, 
                          accepts: 'application/json; charset=utf-8',
                          success: function (data) {
                              if (data.message == 'success') {

                                    $('#errormsg1').css({'color': 'green'});
                                    $("#errormsg1").text("You are succesfully registred!");
                              }else{
                                $('#errormsg1').css({'color': 'red'});
                                $("errormsg1").text("Error 401");
                              }
                          }
                        });
                      });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#submitbtn").click(function(){
                event.preventDefault();
                $.ajax('authorization.php', {
                    type: 'POST',  
                    data: { email: $( "#exampleInputEmail1" ).val(),
                        password:  $( "#exampleInputPassword1" ).val()}, 
                    accepts: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.message == 'success') {
                            
                          $("#sigin").css("display", "none")
                          $("#sigout").css("display", "block")
                            window.location.href = "user_profile.php";
                        }
                    },
                    error: function (errorData, textStatus, errorMessage) {
                        var msg = (errorData.responseJSON != null) ? errorData.responseJSON.errorMessage : ''; 
                        $("#errormsg").text('Incorrect email or password ');

                        $("#errormsg").show();
                    }
                });
            });
        });
    </script>

    <script>
        $( document ).ready(function(){ 

            var a = false;
            var b = false;
          $("#reg").on("click",function(){
            if(a==false){
                $("#regg").css("display","block")
                $("#autt").css("display","none")
                a = true;
                b = false;
            }
            else{                
                $("#regg").css("display","none")
                a = false;
            }

         });
           $("#aut").on("click",function(){
            if(b==false){
                $("#autt").css("display","block")
                $("#regg").css("display","none")
                a = false;
                b = true;
            }
            else{                
                $("#autt").css("display","none")
                b = false;
            }

         });
        });
    </script>
</head>

<body>
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">DEMEU</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">About Us</a>
                    </li>
                    <li>
                        <a href="1.php">Our team</a>
                    </li>
                    <li>
                        <a href="use.php">Useful tips</a>
                    </li>
                    <li id="signin" style="display: block;">
                        <a href="contact.php">Sign in/Sign up</a>
                    </li>
                    <li id="blog" style="display: none;">
                        <a href="blog.php">Blog</a>
                    </li>
                    <li id="myp" style="display: none;">
                        <a href="user_profile.php">My profile</a>
                    </li>
                    <li id="signout" style="display: none;">
                        <a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <header class="intro-header" style="background-image: url('ppp.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Join us</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container" id="autt" style="display: block;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <form>
            <h3>Authorisation</h3><br>
            <span class="error text-danger" id="errormsg" style="display: none"></span>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group col-xs-12">
                <button type="submit" id="submitbtn">Submit</button><span>   </span><a id="reg">Sign up</a>
            </div>
        </form>
    </div>  
</div>
</div>

    <div class="container" id="regg" style="display: none;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <h3>Registration</h3>
                <p>Want to be a part of Demeu? Fill out the form below!</p><br>
                    <p id="errormsg1" style="display: none;"></p>
                <form>
                    <label>Email</label>
                    <input type="text" id="email" placeholder="Enter email" class="form-control">
                    <label>First name</label>
                    <input type="text" id="name" placeholder="Enter First Name" class="form-control">
                    <label>Second Name</label>
                    <input type="text" id="surname" placeholder="Enter Second Name" class="form-control">
                    <label>Password</label>
                    <input type="password" id="password" placeholder="Enter password" class="form-control">
                    <label>Your group</label>
                    <input type="text" id="role" class="form-control">
                    <div class="row"><br>
                        <div class="form-group col-xs-12">
                            <input type="submit" id="regis" value="Register"><span>   </span><a id="aut">Sign in</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
 <hr>
    <footer>
        <div class="container" >
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="https://www.instagram.com/demeu.aitu/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:demeu@gmail.com">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Astana IT University 2020</p>
                </div>
            </div>
        </div>
    </footer>


     


</body>

</html>
<?php
if(isset($_SESSION['user'])){
    echo '<script language="javascript">';
    echo "document.getElementById('signin').style.display = 'none';";
    echo "document.getElementById('signout').style.display = 'block';";
    echo "document.getElementById('myp').style.display = 'block';";
    echo "document.getElementById('blog').style.display = 'block';";
    echo "</script>";
}
    ?>
