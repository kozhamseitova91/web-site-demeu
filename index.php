<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>DEMEU</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/demeu.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
    </script>
   <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    <script src="js.js"></script>
    <style>
      img{
        width: 50%;
      }
    </style>
    <script type="text/javascript">
      $(document).ready(function(){

   $("#img").hover(function() {

    $("#img").animate({width:"150%"}, 700);

    },

    function(){

     $("#img").animate({width:"100%"}, 700);

    }

  );
   $("#img1").hover(function() {

    $("#img1").animate({width:"150%"}, 700);

    },

    function(){

     $("#img1").animate({width:"100%"}, 700);

    }

  );
   $("#img2").hover(function() {

    $("#img2").animate({width:"150%"}, 700);

    },

    function(){

     $("#img2").animate({width:"100%"}, 700);

    }

  );
   $("#img3").hover(function() {

    $("#img3").animate({width:"150%"}, 700);

    },

    function(){

     $("#img3").animate({width:"100%"}, 700);

    }

  );
   $("#img4").hover(function() {

    $("#img4").animate({width:"150%"}, 700);

    },

    function(){

     $("#img4").animate({width:"100%"}, 700);

    }

  );
   $("#img5").hover(function() {

    $("#img5").animate({width:"150%"}, 700);

    },

    function(){

     $("#img5").animate({width:"100%"}, 700);

    }

  );

});

    </script>

</head>

<body>
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">DEMEU</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">About Us</a>
                    </li>
                   <li>
                        <a href="1.php">Our team</a>
                    </li>
                    <li>
                        <a href="use.php">Useful tips</a>
                    </li>
                    <li id="signin" style="display: block;">
                        <a href="contact.php">Sign in/Sign up</a>
                    </li>
                    <li id="blog" style="display: none;">
                        <a href="blog\project.php">Blog</a>
                    </li>
                    <li id="myp" style="display: none;">
                        <a href="user_profile.php">My profile</a>
                    </li>
                    <li id="signout" style="display: none;">
                        <a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header class="intro-header" style="background-image: url('ppp.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>Demeu</h1>
                        <hr class="small">
                        <span class="subheading">Your smile is important<br></span>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="container-fluid container-90">
        <div class="row">

            <div class="container-fluid container-90">
                        <h2>
                            About us
                        </h2>
                        <h3 >
                            We are a club aimed at providing moral support and assistance to our students, because each of us goes through certain difficulties, no matter how trite it sounds. And at this moment, the best thing we can do for each other is to become a support environment that makes us more cohesive and stronger.
Why is mental health important and why should you think about our emotional state?
We believe that moral health has undeniably a direct link with our physical health, school / work, and social life. Therefore, it is very important to have a spiritual balance, so the usual stages in life will seem bright, and success will always be on your side.
Our club would be very pleased to be given the opportunity to help each of you and be useful :)
We hope for your support, because we are one family where everyone should take care of each other 💞
All good ❣️
                        </h3>
                </div>
            </div>
        </div>
    </div>

<br><hr>


      <div class="container" >
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center">
            <h2 style="margin-left: 50%">Our actitvities</h2><br><br>
          </div>
        </div>
        <div style="margin-left: 10%; display: grid; grid-template-columns: repeat(3, 1fr);">

          <div>
            <div id="img">
              <img src="pictures/1.jpg" alt="Image"  >
              <p><a href="https://www.instagram.com/p/B46qXjOHAve/">For more information </a></p>
            </div> 
          </div>
          <div>
            <div id="img1">
              <img src="pictures/2.jpg" alt="Image"  >
              <p><a href="https://www.instagram.com/p/B47H1USnCDF/">For more information </a></p>
            </div>
          </div>
          <div>
            <div id="img2" >
              <img src="pictures/3.jpg" alt="Image" >
              <p><a href="https://www.instagram.com/p/B4fRslkDuMK/">For more information </a></p>
            </div> 
          </div>
           <div >
            <div id="img3"  >
              <img src="pictures/4.jpg" alt="Image" >
              <p><a href="https://www.instagram.com/p/B4fRslkDuMK/">For more information </a></p>
            </div> 
          </div>
          <div>
            <div id="img4" >
              <img src="pictures/5.jpg" alt="Image" >
              <p><a href="https://www.instagram.com/p/B4J9eO0F-FS/">For more information </a></p>
            </div>
          </div>
          <div>
            <div id="img5">
              <img src="pictures/6.jpg" alt="Image" >
              <p><a href="https://www.instagram.com/p/B7lExNUnHIi/">For more information </a></p>
            </div> 
          </div>
          </div>
          
        </div>
      </div>


    <hr>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="https://www.instagram.com/demeu.aitu/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:demeu@gmail.com">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Astana IT University 2020</p>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
<?php
if(isset($_SESSION['user'])){
    echo '<script language="javascript">';
    echo "document.getElementById('signin').style.display = 'none';";
    echo "document.getElementById('signout').style.display = 'block';";
    echo "document.getElementById('myp').style.display = 'block';";
    echo "document.getElementById('blog').style.display = 'block';";
    echo "</script>";
}