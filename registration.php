<?php
  header('Content-Type: application/json');

  if(!empty($_POST["email"]) and !empty($_POST["name"]) and !empty($_POST["surname"]) and 
  !empty($_POST["password"]) and !empty($_POST["role"])){

      require_once "link.php";

      $stmt = $link->prepare("INSERT INTO demeu(email,name,surname,password,role) VALUES
      (?,?,?,?,?)");
      $stmt->bind_param("sssss",$_POST['email'],$_POST["name"],$_POST['surname'],$_POST['password'],$_POST['role']);

      if($stmt->execute()){
        $return = array('message'=>'success');
      }
      else{
        $return = array('message'=>'error');
      }
      echo json_encode($return);
  }

 ?>
