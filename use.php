<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>Useful tips</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/demeu.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
      <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    <script src="js.js"></script>
    <script>
        $( document ).ready(function(){ 

            var a = false;
            var b = false;
            var c = false;
          $("#r").on("click",function(){
            if(a==false){
                $("#rr").css("display","block")
                $("#ll").css("display","none")
                $("#ww").css("display","none")
                a = true;
                b = false;
                c = false;
            }
            else{                
                $("#rr").css("display","none")
                a = false;
            }

         });
           $("#l").on("click",function(){
            if(b==false){
                $("#ll").css("display","block")
                $("#rr").css("display","none")
                $("#ww").css("display","none")
                a = false;
                b = true;
                c = false;
            }
            else{                
                $("#ll").css("display","none")
                b = false;
            }

         });
            $("#w").on("click",function(){
            if(c==false){
                $("#ww").css("display","block")
                $("#ll").css("display","none")
                $("#rr").css("display","none")
                c = true;
                a = false;
                b = false;
            }
            else{                
                $("#ww").css("display","none")
                c = false;
            }

         });
        });
    </script>
</head>

<body>
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">DEMEU</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">About Us</a>
                    </li>
                    <li>
                        <a href="1.php">Our team</a>
                    </li>
                    <li>
                        <a href="use.php">Useful tips</a>
                    </li>
                    <li id="signin" style="display: block;">
                        <a href="contact.php">Sign in/Sign up</a>
                    </li>
                    <li id="blog" style="display: none;">
                        <a href="blog\project.php">Blog</a>
                    </li>
                    <li id="myp" style="display: none;">
                        <a href="user_profile.php">My profile</a>
                    </li>
                    <li id="signout" style="display: none;">
                        <a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header class="intro-header" style="background-image: url('ppp.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>Useful tips</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
     <div class="www" style="margin-left: 39%">
        <input type="submit" value="For reading" id="r">
        <input type="submit" value="For listening" id="l">
        <input type="submit" value="For watching" id="w">

    </div><br>

    <div class="site-section border-bottom" id="rr" style="display: none">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
              <img src="pictures/0.png" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
              <img src="pictures/0.png" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
              <img src="pictures/0.png" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
        </div>
      </div>
    </div>

     <div class="site-section border-bottom" id="ll" style="display: none">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
              <img src="pictures/00.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
              <img src="pictures/00.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
              <img src="pictures/00.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section border-bottom" id="ww" style="display: none">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
              <img src="pictures/000.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
              <img src="pictures/000.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
              <img src="pictures/000.jpg" style=" border-radius: 5px; width: 15%; height: 15%">
              <p>sdb sdkfjskfj skjskfnsr skjfbksrfu kvjnskjfrb skfj</p>
          </div>
        </div>
      </div>
    </div>
      <hr>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="https://www.instagram.com/demeu.aitu/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:demeu@gmail.com">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Astana IT University 2020</p>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
<?php
if(isset($_SESSION['user'])){
    echo '<script language="javascript">';
    echo "document.getElementById('signin').style.display = 'none';";
    echo "document.getElementById('signout').style.display = 'block';";
    echo "document.getElementById('myp').style.display = 'block';";
    echo "document.getElementById('blog').style.display = 'block';";
    echo "</script>";
}