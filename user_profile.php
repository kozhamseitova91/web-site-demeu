<?php
session_start();

if (!isset($_SESSION['user'])) {
    header("Location: contact.php");
    return;
}

require_once "link.php";
$stmt = $link->prepare("SELECT * FROM demeu WHERE email = ?");
$stmt->bind_param("s", $_SESSION['user']['email']);
/* execute query */
$stmt->execute();

/* Get the result */
$result = $stmt->get_result();

$row = $result->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/demeu.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>

    <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    <script src="js.js"></script>
</script>
</head>
<body>
     <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">DEMEU</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">About Us</a>
                    </li>
                    <li>
                        <a href="1.html">Our team</a>
                    </li>
                    <li>
                        <a href="use.html">Useful tips</a>
                    </li>
                    <li id="signin" style="display: none;">
                        <a href="contact.php">Sign in/Sign up</a>
                    </li>
                    <li id="blog" style="display: block;">
                        <a href="blog\project.php">Blog</a>
                    </li>
                    <li id="myp" style="display: block;">
                        <a href="../user_profile.php">My profile</a>
                    </li>
                    <li id="signout" style="display: block;">
                        <a href="signout.php">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <header class="intro-header" style="background-image: url('ppp.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Join us</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
<div class="container">
    <div class="row">
        <div class="col-md-6" id="user_info">
            <div class="col-md-6 details">
                <blockquote>
                    <h5 id="fullname"><?php echo $row['name'] . ' ' . $row['surname'] ?></h5>
                </blockquote>
                <div id="email"><span><?php echo $row['email'] ?></span></div><br>
                <div id="role"><span><?php echo $row['role'] ?></span></div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
